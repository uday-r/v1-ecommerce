import { User, Role, Furniture } from './connector';

const resolvers = {
  Query: {
    getUser(_, args) {
      return User
        .findOne({name: args.name})
        .populate('roles favoriteFurnitures')
        .then((user) => ({
          ...user.toObject(),
          id: user._id,
        })
      );
    },
    furnitures(_, args) {
      return Furniture.find();
    }
  },
  Mutation: {
    addFavFurniture: async (parent, args, dp) => {
      return await User.update({
        _id: args.userId
      }, {
        '$push': {
          "favoriteFurnitures": args.furnitureId,
        }
      }).then(() => Furniture.findOne({
        _id: args.furnitureId,
      }));
    }
  },
  User: {
    roles({roles}) {
      return roles.map((role) => {
        if(typeof(role.id) == 'undefined') {
          role.id = role._id;
          delete role._id;
        }
        return role;
      });
    },
    favoriteFurnitures({favoriteFurnitures}) {
      return favoriteFurnitures.map((furniture) => {
        if(typeof(furniture.id) == 'undefined') {
          furniture.id = furniture._id;
          delete furniture._id;
        }
        return furniture;
      });
    }
  }
}

export default resolvers;