const users = [
  {
    name: 'Uday Reddy',
    profilePic: 'https://avatars3.githubusercontent.com/u/2574814?v=4',
  },
  {
    name: 'Pablo Escobar',
    profilePic: 'https://s24193.pcdn.co/wp-content/uploads/2017/08/candice-horde-entitymag-@pixelmovecom-696x720.png',
  },
];

const roles = [
  {
    name: 'ROLE_USER',
  },
  {
    name: 'ROLE_ADMIN',
  },
];

const furnitures = [
  {
    name: 'Abbey Solid Wood Arm Chair In Provincial Teak Finish',
    image: 'https://ii2.pepperfry.com/media/catalog/product/a/b/236x260/abbey-solid-wood-arm-chair-in-provincial-teak-finish-by-woodsworth-abbey-solid-wood-arm-chair-in-pro-44sv4f.jpg',
    price: 5000,
    description: "The Abbey is designed with vertical bands that accentuate the grain of Sheesham Wood.",
  },
  {
    name: 'Segur Solid Wood Arm Chair In Provincial Teak Finish',
    image: 'https://ii3.pepperfry.com/media/catalog/product/s/e/236x260/segur-solid-wood-arm-chair-in-provincial-teak-finish-by-woodsworth-segur-solid-wood-arm-chair-in-pro-19v68v.jpg',
    price: 500,
    description: "The Abbey is designed with vertical bands that accentuate the grain of Sheesham Wood.",
  },
  {
    name: 'Lennox Metal Chair In White Colour',
    image: 'https://ii1.pepperfry.com/media/catalog/product/l/e/236x260/lennox-metal-chair-in-white-color-by-bohemiana-lennox-metal-chair-in-white-color-by-bohemiana-bwaa4l.jpg',
    price: 2500,
    description: "The Abbey is designed with vertical bands that accentuate the grain of Sheesham Wood.",
  },
  {
    name: 'Ekati Metal Chair In Distress Red Colour With Wooden Seat',
    image: 'https://ii2.pepperfry.com/media/catalog/product/e/k/236x260/ekati-metal-chair-in-distress-red-color-with-wooden-seat-by-bohemiana-ekati-metal-chair-in-distress--rgtcq5.jpg',
    price: 1400,
    description: "The Abbey is designed with vertical bands that accentuate the grain of Sheesham Wood.",
  },
]


export {
	users,
	roles,
  furnitures,
};
