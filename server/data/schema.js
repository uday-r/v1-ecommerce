import { makeExecutableSchema, addMockFunctionsToSchema } from 'graphql-tools';
import resolvers from './resolvers';

const typeDefs = `
	type Query {
    getUser(name: String!): User!
    furnitures: [Furniture]
  }

  type Mutation {
    addFavFurniture(userId: String!, furnitureId: String!): Furniture
  }

  type User {
    id: String
    name: String
    profilePic: String
    roles: [Role]
    favoriteFurnitures: [Furniture]
  }

  type Role {
    id: String!
    name: String!
  }

  type Furniture {
    id: String!
    name: String!
    image: String!
    price: Int!
    description: String!
  }
`;

const schema = makeExecutableSchema({ typeDefs, resolvers });

export default schema;
