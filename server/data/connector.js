import casual from 'casual';
import fetch from 'node-fetch';
import Mongoose from 'mongoose';
import _ from 'lodash';
import { users, roles, furnitures } from './mocks';



Mongoose.Promise = global.Promise;

const mongo = Mongoose.connect('mongodb://localhost/furlanco', {
  useMongoClient: true
});


const UserSchema = Mongoose.Schema({
  name: String,
  profilePic: String,
  roles: [{
  	type: Mongoose.Schema.Types.ObjectId,
  	ref: 'role',
  }],
  favoriteFurnitures: [{
  	type: Mongoose.Schema.Types.ObjectId,
  	ref: 'furniture',
  }],
});
const User = Mongoose.model('user', UserSchema);

const RoleSchema = Mongoose.Schema({
  name: String,
});
const Role = Mongoose.model('role', RoleSchema);

const FurnitureSchema = Mongoose.Schema({
  name: String,
  image: String,
  price: Number,
  description: String,
});
const Furniture = Mongoose.model('furniture', FurnitureSchema);

async function loadUsers() {
	const dbUsers = await User.find();
	let userRoles;
	if(dbUsers.length === 0) {
		const savedRoles = await Role.create(roles);
		await Furniture.create(furnitures);
		users.forEach(async(user, id) => {
			if(id == 0) {
				user.roles = [savedRoles[0]._id];
			} else {
				user.roles = savedRoles.map((savedRole) => savedRole._id);
			}
			await User
				.create(user);
		});
		
	}
}

loadUsers();

export {
	User,
	Role,
	Furniture,
}