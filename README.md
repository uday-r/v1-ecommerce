## Running project



**Prerequisites to run project**

Make sure you have `node` and `mongo`.




**Next:**

`cd client`

`npm install`

`npm start`

`cd ../server`

`npm install`

`npm start`




Open up `http://localhost:7000/`.


#### UI technologies used
1. react-16.
2. redux-6.
3. react-apollo-2.3.
4. reactstrap-7.

Using apollo to make `query` and `mutation` to backend.


#### App walkthrough:
1. On loading the app a list of available furnitures is visible.
2. You can click on the login button and login as user or admin.
3. Once you login you'll be routed back to home page with a new button `Set as Favorite`.
4. For admin ussers `user management` appears on header(Non functional).

### TODO
1. Persisting login session information into browser.
2. Use authentication token to authorize backend requests.
3. On hitting `Set as Favorite` button show `Remove from my favorites` button.
4. Add `List Favorites` under account settings.
5. Make login dynamic by setting username and password, allow social media login.
6. Allow admin user to create/edit/block users.
7. Remove redux state management and use apollo state.
8. Dockerize both client and server and create different repos.
9. Create project structure similar to https://bitbucket.org/account/user/uday-alone/projects/WEB.