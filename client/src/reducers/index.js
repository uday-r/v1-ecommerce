const v1EcommerceApp = (state = {}, action) => {
  switch (action.type) {
    case 'LOGIN_AS_USER':
      return {
        ...state,
        loggedInUser: {
          ...action.loggedInUser
        },
      };
    case 'LOGOUT':
      return {
        ...state,
        loggedInUser: null,
      };
    case 'INIT_FURNITURES':
      return {
        ...state,
        furnitures: [
          ...action.furnitures,
        ]
      };
    case 'ADD_FAVORITE_FURNITURE':
      return {
        ...state,
        loggedInUser: {
          ...state.loggedInUser,
          favoriteFurnitures: state
            .loggedInUser
            .favoriteFurnitures
            .concat([ action.furniture ]),
        }
      };
    case 'ADD_NOTIFICATION':
      return {
        ...state,
        notificationMessage: action.message,
      };
    case 'CLEAR_NOTIFICATION':
      return {
        ...state,
        notificationMessage: null,
      }
    default:
      return state
  }
}

export default v1EcommerceApp;