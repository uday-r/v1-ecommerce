
const loginAs = loggedInUser => ({
  type: 'LOGIN_AS_USER',
  loggedInUser
});

const logout = () => ({
  type: 'LOGOUT',
});

const initFurnitures = furnitures => ({
	type: 'INIT_FURNITURES',
	furnitures
})

const setFavoriteFurniture = furniture => ({
	type: 'SET_FAVORITE_FURNITURE',
	furniture,
})

const addNotification = message => ({
	type: 'ADD_NOTIFICATION',
	message,
})

const clearNotification = () => ({
  type: 'CLEAR_NOTIFICATION',
})

export {
  loginAs,
  logout,
  initFurnitures,
  setFavoriteFurniture,
  addNotification,
  clearNotification,
}
