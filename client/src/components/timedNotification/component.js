import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Alert } from 'reactstrap';


class TimedNotification extends Component {

	static propTypes = {
    notificationMessage: PropTypes.string,
    clearNotification: PropTypes.func,
  }

  componentWillUnmount() {
    clearTimeout(this.timer);
  }

  render() {
  	const {
  		notificationMessage,
  	} = this.props;

    this.timer = setTimeout(() => {
      this.props.clearNotification();
      clearTimeout(this.timer);
    }, 5000);

    if(!notificationMessage) {
      return <div />;
    }

  	return (<Alert color="primary">
      {notificationMessage}
    </Alert>)
  }

}

export default TimedNotification;
