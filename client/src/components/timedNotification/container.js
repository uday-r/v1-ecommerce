import { connect } from 'react-redux'
import Component from './component'
import { clearNotification } from './../../actions/'

const mapStateToProps = ({
		notificationMessage,
	}, ownProps) => {
  return {
  	notificationMessage,
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    clearNotification: () => dispatch(clearNotification()),
  }
}


export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Component);