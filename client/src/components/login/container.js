import { connect } from 'react-redux'
import { loginAs } from './../../actions/'
import Component from './component'

const mapStateToProps = ({loggedInUser}, ownProps) => {
  return {
    loggedInUser
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    loginAs: (user) => dispatch(loginAs(user)),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Component);