import React, { Component } from 'react';
import gql from 'graphql-tag';
import { withRouter } from 'react-router';
import client from '../../apollo/';
import './../../App.css';
import {
  Jumbotron,
  Button,
  Col,
  Row,
} from 'reactstrap';

const GET_USERS = gql`
  query($name: String!) {
    getUser(name: $name) {
      id
      name
      roles {
        id
        name
      }
      favoriteFurnitures {
        id
        name
        image
        price
        description
      }
    }
  }
`;

class LoginPage extends Component {

  loginAs(userId) {
    let name;
    if(userId === 1){
      name = 'Uday Reddy';
    } else {
      name = 'Pablo Escobar';
    }
    client.query({
      query: GET_USERS,
      variables: {
        name,
      },
    }).then(({
      data: {
        getUser
      }
    }) => {
      if(getUser) {
        this.props.loginAs(getUser);
        this.props.history.push('/');
      }
    });
  }

  render() {
    return (
      <Jumbotron>
          <Row>
            <Col className="text-center">
              <Button onClick={e => {
                  e.preventDefault()
                  this.loginAs(1)
                }}>Login as user</Button>
            </Col>
          </Row>
          <Row className="padding-top-1">
            <Col className="text-center">
              <Button onClick={e => {
                  e.preventDefault()
                  this.loginAs(2)
                }}>Login as admin</Button>
            </Col>
          </Row>
      </Jumbotron>
    );
  }
}

export default withRouter(LoginPage);