import { connect } from 'react-redux'
import component from './component'
import { setFavoriteFurniture, addNotification } from './../../actions/'

const mapStateToProps = (state, ownProps) => {
  return {
    ...ownProps
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    setFavoriteFurniture: (userId, furniture) => dispatch(setFavoriteFurniture(userId, furniture)),
    addNotification: (message) => dispatch(addNotification(message)),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(component);