import React, { Component } from 'react';
import PropTypes from 'prop-types';
import gql from 'graphql-tag';
import './../../App.css';
import client from '../../apollo/';
import {
  Card, CardText, CardBody, CardLink,
  CardTitle, CardSubtitle,
} from 'reactstrap';

const TOGGLE_FAVORITE_FURNITURE = gql`
  mutation($userId: String!, $furnitureId: String!) {
    addFavFurniture(
      userId: $userId,
      furnitureId: $furnitureId
    ) {
      id
      name
      image
      price
      description
    }
  }
`;

class FurnitureCard extends Component {
  static propTypes = {
    details: PropTypes.shape({
      id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      price: PropTypes.number.isRequired,
      description: PropTypes.string.isRequired,
      image: PropTypes.string.isRequired,
    }),
    userId: PropTypes.string,
    setFavoriteFurniture: PropTypes.func,
    addNotification: PropTypes.func,
  }

  setFavoriteFurniture(userId, furnitureId, e) {
    client.mutate({
      mutation: TOGGLE_FAVORITE_FURNITURE,
      variables: {
        userId,
        furnitureId,
      },
    }).then(({data: { addFavFurniture } }) => {
      this.props.setFavoriteFurniture(userId, addFavFurniture)
      this.props.addNotification(`Added ${addFavFurniture.name} to your favorites.`);
    })
  }

  render() {
    const {
      details,
      userId,
    } = this.props;

    return (
      <Card className="col-md-3 col-12">
        <CardBody>
          <CardTitle>{details.name}</CardTitle>
          <CardSubtitle>₹{details.price}</CardSubtitle>
        </CardBody>
        <img width="100%" src={details.image} alt="Furniture" />
        <CardBody>
          <CardText>{details.description}</CardText>
          {userId &&
            <CardLink
              onClick={(e) => this.setFavoriteFurniture(userId, details.id, e)}
              href="#">
              Set as Favorite
            </CardLink>
          }
        </CardBody>
      </Card>
    );
  }
}

export default FurnitureCard;