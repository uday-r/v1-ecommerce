import React from 'react';

import ReactDOM from 'react-dom';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });
const details = {
  id: '212313',
  name: 'Sheesham',
  price: 1000,
  image: 'https://test.com',
  description: 'blah blah',
}
const mockRetData = {
  data: {
    addFavFurniture: {
      test: 'id'
    }
  }
};
jest.mock('../../apollo/', function() {
  return {
    mutate: () => Promise.resolve(mockRetData)
  };
})

import FurnitureCard from './component';

it('renders without crashing', () => {
  const div = document.createElement('div');
  const component = <FurnitureCard details={details} />;
});

it('renders the details properly', () => {
  const component = <FurnitureCard details={details} />;
  const wrapper = shallow(component);
  expect(wrapper.contains('Set as Favorite')).toBe(false);
  Object
    .keys(details)
    .forEach((key) => {
      if(key === 'id' || key === 'image') {
        return;
      }
      expect(wrapper.contains(details[key])).toBe(true);
    });
  
});

it('should click card link', () => {
  const userId = "test-user-id";

  const props = {
    userId,
    details,
    setFavoriteFurniture: jest.fn(),
    addNotification: jest.fn(),
  }  
  const component = <FurnitureCard {...props} />;
  const shallowWrapper = shallow(component);

  expect(shallowWrapper.contains('Set as Favorite')).toBe(true);
  shallowWrapper.find('CardLink').simulate('click');
  process.nextTick(() => {
    expect(shallowWrapper.instance().props.setFavoriteFurniture).toHaveBeenCalled();
    expect(shallowWrapper.instance().props.addNotification).toHaveBeenCalled();
  });
});