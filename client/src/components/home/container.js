import { connect } from 'react-redux'
import Component from './component'
import { initFurnitures } from './../../actions/'

const mapStateToProps = ({
		loggedInUser,
		furnitures,
	}, ownProps) => {
  return {
  	loggedInUser,
    furnitures,
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
  	loadFurnitures: (furnitures) => dispatch(initFurnitures(furnitures)),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Component);