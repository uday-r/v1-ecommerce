import React, { Component } from 'react';
import gql from 'graphql-tag';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router';
import client from '../../apollo/';
import './../../App.css';
import FurnitureCard from './../furnitureCard/';
import TimedNotification from './../timedNotification/';

const GET_FURNITURES = gql(`
  {
    furnitures {
      id
      name
      price
      image
      description
    }
  }
`);

class HomePage extends Component {

  static propTypes = {
    loggedInUser: PropTypes.shape({
      id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      roles: PropTypes.arrayOf(
        PropTypes.shape({
          id: PropTypes.string.isRequired,
          name: PropTypes.string.isRequired,
        }),
      ),
    }),
    furnitures: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
        description: PropTypes.string.isRequired,
        image: PropTypes.string.isRequired,
      }),
    ),
  }

  componentDidMount() {
    this.loadFurnitures()
  }

  loadFurnitures() {
    client.query({
      query: GET_FURNITURES,
    }).then(
      ({ data: { furnitures }, loading }) => {
        if(furnitures) {
          this.props.loadFurnitures(furnitures);
        }
      }
    );
  }

  render() {
    const {
      furnitures,
      loggedInUser,
    } = this.props;
    const userId = loggedInUser && loggedInUser.id? loggedInUser.id: null;

    if(!furnitures || typeof(furnitures) === 'undefined') {
      return (<h3>Loading</h3>);
    }
    return (
      <div className="container">
        <TimedNotification />
        <div className="row">
          {
            furnitures.map(furniture => (
              <FurnitureCard key={furniture.id}
                details={furniture}
                userId={userId}/>
            ))
          }
        </div>
      </div>
    );
  }
}

export default withRouter(HomePage);