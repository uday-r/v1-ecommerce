import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import {
  Navbar,
  NavbarBrand,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Collapse,
  Nav,
  Button,
} from 'reactstrap';

export default class Header extends Component {
  static propTypes = {
    loggedInUser: PropTypes.shape({
      id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      roles: PropTypes.arrayOf(
        PropTypes.shape({
          id: PropTypes.string.isRequired,
          name: PropTypes.string.isRequired,
        }),
      ),
    }),
    logout: PropTypes.func.isRequired,
  }

  userIsAdmin() {
    return this.props.loggedInUser
      && this.props.loggedInUser.roles.find((it) => it.name === 'ROLE_ADMIN');
  }

  render() {
    const {
      loggedInUser,
      logout,
    } = this.props;
    return (
      <div>
        <Navbar color="light" light expand="md">
          <NavbarBrand href="/">Furlanco</NavbarBrand>
          {this.userIsAdmin() && (
            <Button>
              User Management
            </Button>
          )}
          <Collapse navbar>
            <Nav className="ml-auto" navbar>
              {
                loggedInUser?
                  (
                    <UncontrolledDropdown nav inNavbar>
                      <DropdownToggle nav caret>
                        {loggedInUser.name}
                      </DropdownToggle>
                      <DropdownMenu right>
                        <DropdownItem>
                          Account Settings
                        </DropdownItem>
                        <DropdownItem divider />
                        <DropdownItem onClick={logout}>
                          Logout
                        </DropdownItem>
                      </DropdownMenu>
                    </UncontrolledDropdown>
                  )
                  :
                  (
                    <Link to="/login"><Button>Login</Button></Link>
                  )
              }
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
}
