import { connect } from 'react-redux'
import component from './component'
import { logout } from './../../actions/'

const mapStateToProps = ({loggedInUser}, ownProps) => {
  return {
    loggedInUser
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    logout: () => dispatch(logout())
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(component);