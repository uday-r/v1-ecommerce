import React, { Component } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Route } from "react-router-dom";
import Header from './components/header/'
import Login from './components/login/'
import Home from './components/home/'

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <Header />
          <Route path="/" exact component={Home} />
          <Route path="/login" exact component={Login} />
        </div>
      </Router>
    );
  }
}

export default App;
